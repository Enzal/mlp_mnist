import os
import sys
import torch
import numpy as np
from torchvision import datasets
import torchvision.transforms as transforms
from torch.utils.data.sampler import SubsetRandomSampler
import torch.nn as nn
import torch.nn.functional as func
import matplotlib.pyplot as plt


# definition du nn
class Mlp(nn.Module):
    def __init__(self):
        super(Mlp, self).__init__()
        hidden_1 = 256
        hidden_2 = 256
        self.fc1 = nn.Linear(28 * 28, 256)
        self.fc2 = nn.Linear(256, 256)
        self.fc3 = nn.Linear(256, 10)
        self.droput = nn.Dropout(0.2)

    def forward(self, x):
        x = x.view(-1, 28 * 28)
        x = func.relu(self.fc1(x))
        x = self.droput(x)
        x = func.relu(self.fc2(x))
        x = self.droput(x)
        x = self.fc3(x)
        return x


if len(sys.argv) == 2:
    nb_cycles = int(sys.argv[1])
else:
    print('Entrez le nombre de cycle a effectuer en argument.')
    exit(0)

nb_workers = 0
batch_size = 20
valid_size = 0.2

test_results = list()

# chargement du datasets
train_data = datasets.MNIST(root='data', train=True, download=True, transform=transforms.ToTensor())
test_data = datasets.MNIST(root='data', train=False, download=True, transform=transforms.ToTensor())

# indices necessaires pout la validation
nb_train = len(train_data)
indices = list(range(nb_train))
np.random.shuffle(indices)
split = int(np.floor(valid_size * nb_train))
train_index, valid_index = indices[split:], indices[:split]

# definition des samplers
train_sampler = SubsetRandomSampler(train_index)
valid_sampler = SubsetRandomSampler(valid_index)

# initialisation des dataloaders
train_loader = torch.utils.data.DataLoader(
    train_data,
    batch_size=batch_size,
    sampler=train_sampler,
    num_workers=nb_workers,
)
valid_loader = torch.utils.data.DataLoader(
    train_data,
    batch_size=batch_size,
    sampler=valid_sampler,
    num_workers=nb_workers,
)
test_loader = torch.utils.data.DataLoader(
    test_data,
    batch_size=batch_size,
    num_workers=nb_workers,
)

# initialisation du nn, de la fonction de perte et de l'optimiseur
model = Mlp()
loss_fn = nn.CrossEntropyLoss()
optimizer = torch.optim.SGD(model.parameters(), lr=0.01)


valid_loss = np.Inf
for epoch in range(nb_cycles):
    train_loss = 0
    valid_loss_new = 0

    # entrainement du modèle
    model.train()
    for data, label in train_loader:
        optimizer.zero_grad()
        output = model(data)
        loss = loss_fn(output, label)
        loss.backward()
        optimizer.step()
        train_loss += loss.item() * data.size(0)

    # validation du modèle
    model.eval()
    for data, label in valid_loader:
        output = model(data)
        loss = loss_fn(output, label)
        valid_loss_new = loss.item() * data.size(0)

    # recalcul de la perte
    train_loss = train_loss / len(train_loader.sampler)
    valid_loss_new = valid_loss_new / len(valid_loader.sampler)

    print('Epoch: {} Perte: {:.6f}'.format(epoch + 1, valid_loss_new))

    if valid_loss_new <= valid_loss:
        print('\tDiminutuion de la perte ({:.6f} --> {:.6f}).Sauvegarde du modèle.'.format(valid_loss, valid_loss_new))
        torch.save(model.state_dict(), 'model.pt')
        valid_loss = valid_loss_new

    model.load_state_dict(torch.load('model.pt'))
    # initialisation des listes pour le suivit des performances du modèle
    test_loss = 0.0
    class_correct = list(0. for i in range(10))
    class_total = list(0. for i in range(10))

    # evaluation du modèle sur de nouvelles données
    model.eval()
    for data, target in test_loader:
        output = model(data)
        loss = loss_fn(output, target)
        test_loss += loss.item() * data.size(0)
        _, pred = torch.max(output, 1)
        correct = np.squeeze(pred.eq(target.data.view_as(pred)))
        for i in range(len(target)):
            label = target.data[i]
            class_correct[label] += correct[i].item()
            class_total[label] += 1

    # predictions du modèle
    print('\tCalcul des prédictions....')
    nb_mismatch = 0
    dataiter = iter(test_loader)
    for i in range(20):
        images, labels = dataiter.next()
        output = model(images)
        _, preds = torch.max(output, 1)

        # calcul des erreurs
        for idx in np.arange(20):
            if preds[idx] != labels[idx]:
                nb_mismatch += 1
    test_results.append(nb_mismatch)

print('Suppression du modèle.')
os.unlink('model.pt')

# affichage des résultats
plt.plot(range(1, nb_cycles + 1), test_results, 'r-o')
plt.xlabel('Epoch')
plt.ylabel('Nombre d\'erreurs sur 400')
plt.grid(axis='y', linestyle='dotted')
plt.show()
