# MLP MNIST

## Introduction
The goal of this project was to setup and test the efficiency of a simple multilayer perceptron.
The neural network is trained on the MNIST dataset.
It's ability to class images in their respective categories is mesured throughout the training.
A graph presenting the results is then plotted and displayed at the end

## Test the program
First install the needed python libraries :
```
pip install numpy matplotlib torch torchvision
```
Then run mlp.py with the desired number of epochs as the first argument.
You can see how the training goes by trying different values.
For example you can start with :
```
python ./mlp.py 10
```

## Notes
mlp_fresh_model.py is a modified version of mlp.py, it creates a new model at each cycle so it's considerably slower.
It is still interesting though because it can demonstrate the randomness of machine learning algorithms.
